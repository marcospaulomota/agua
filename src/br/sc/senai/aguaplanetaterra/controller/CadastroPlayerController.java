package br.sc.senai.aguaplanetaterra.controller;

import java.io.Serializable;

import javax.enterprise.context.SessionScoped;
import javax.inject.Named;



@Named(value="cadastroPlayerController")
@SessionScoped
public class CadastroPlayerController implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String nomeJogador;
	private String sobrenomeJogador;
	private String idade;

	private Integer numeroLetras;
	
	private String parOuImpar;
	
	public String getParOuImpar() {
		return parOuImpar;
	}

	public void setParOuImpar(String parOuImpar) {
		this.parOuImpar = parOuImpar;

	}

	public void contarLetras() {
		numeroLetras = nomeJogador.length()+sobrenomeJogador.length();
		
		if (numeroLetras % 2 == 0){
			parOuImpar = "Par";
		} else {
			parOuImpar = "Impar";
		}
		
	}
	
	public String getNomeJogador() {
		return nomeJogador;
	}

	public void setNomeJogador(String nomeJogador) {
		this.nomeJogador = nomeJogador;
	}

	public String getSobrenomeJogador() {
		return sobrenomeJogador;
	}

	public void setSobrenomeJogador(String sobrenomeJogador) {
		this.sobrenomeJogador = sobrenomeJogador;
	}

	public String getIdade() {
		return idade;
	}

	public void setIdade(String idade) {
		this.idade = idade;
	}

	public Integer getNumeroLetras() {
		return numeroLetras;
	}

	public void setNumeroLetras(Integer numeroLetras) {
		this.numeroLetras = numeroLetras;
	}
	
	

}
