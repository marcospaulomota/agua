package br.sc.senai.aguaplanetaterra.controller;

import java.io.Serializable;

import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

@Named(value="paginaInicialControler")
@SessionScoped
public class paginaInicialController implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String dizeres = "Reduza o consumo de água. O planeta agradece!";

	public String getDizeres() {
		return dizeres;
	}

	public void setDizeres(String dizeres) {
		this.dizeres = dizeres;
	}
	

}
